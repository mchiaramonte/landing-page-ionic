FROM node:alpine AS builder

RUN npm i -g npm@7.5.6

RUN npm install -g @ionic/cli

WORKDIR /usr/src/app

COPY ./source/package.json .
COPY ./source/package-lock.json .

RUN npm ci

COPY ./source .

RUN ionic build --prod

FROM nginx:alpine

RUN rm -rf /usr/share/nginx/html/*

COPY nginx.conf /etc/nginx/nginx.conf

COPY --from=builder /usr/src/app/www /usr/share/nginx/html

EXPOSE 80
