Set-Location $PSScriptRoot

docker compose build portfolio

docker image tag ghcr.io/chiaramontemarcello/control-panel/portfolio:latest ghcr.io/chiaramontemarcello/control-panel/portfolio:1.0.6

docker push ghcr.io/chiaramontemarcello/control-panel/portfolio:latest
docker push ghcr.io/chiaramontemarcello/control-panel/portfolio:1.0.6
