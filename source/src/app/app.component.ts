import { Component } from '@angular/core';
import { MenuController, Platform } from '@ionic/angular';
import { filter, map, switchMap } from 'rxjs/operators';

export interface MenuPage {
    title: string;
    url: string;
    icon: string;
}

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent {
    menuTitle = '';
    linkedinAccount = 'https://www.linkedin.com/in/marcello-chiaramonte/';
    xingAccount = 'https://www.xing.com/profile/Marcello_Chiaramonte/cv';
    email = 'mailto: marcello@chiaramonte.me';
    toolbarShown = false;

    appPages: MenuPage[] = [
        {
            title: 'About',
            url: '/about',
            icon: 'person',
        },
        {
            title: 'Projects',
            url: '/projects',
            icon: 'apps',
        },
        {
            title: 'Skills',
            url: '/skills',
            icon: 'code',
        },
    ];

    constructor(public menu: MenuController) {}
}
