import { filter, map, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

export interface Message {
    fromName: string;
    subject: string;
    date: string;
    id: number;
    read: boolean;
}

export interface Project {
    title: string;
    description: string;
}

export interface WorkExperience {
    language: string;
    period: string;
    workplace: string[];
    description: string;
    achievements: string[];
    image: string;
    location: string[];
}

@Injectable({
    providedIn: 'root',
})
export class DataService {
    language = 'EN';
    constructor(private http: HttpClient) {}

    getWorkExperience(): Observable<WorkExperience[]> {
        return this.http
            .get<WorkExperience[]>('assets/data/work-experience.json')
            .pipe(map((xp) => xp.filter((x) => x.language === this.language)));
    }
}
