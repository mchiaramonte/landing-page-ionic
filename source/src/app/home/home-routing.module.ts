import { HomePage } from './home.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: 'scan',
        loadChildren: () =>
            import('../scan-me/scan-me.module').then((m) => m.ScanMePageModule),
    },
    {
        path: '',
        component: HomePage,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class HomePageRoutingModule {}
