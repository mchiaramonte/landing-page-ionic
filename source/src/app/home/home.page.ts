import { Component, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { DataService, WorkExperience } from '../services/data.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {
    @ViewChild('content') private content: IonContent;

    workExperienceList: WorkExperience[] = [];
    dataLoading = true;

    listItemsOpen: Set<string> = new Set();

    constructor(private data: DataService) {
        this.data.getWorkExperience().subscribe({
            next: (works) => {
                console.table(works);
                this.workExperienceList = works;
                this.dataLoading = false;
            },
        });
    }

    toggleItem(id: string) {
        if (this.listItemsOpen.has(id)) {
            this.listItemsOpen.delete(id);
        } else {
            this.listItemsOpen.add(id);

            setTimeout(() => {
                if (this.shouldScroll(id)) {
                    const el = document.getElementById(id);
                    this.content.scrollByPoint(0, el.clientHeight + 100, 700);
                }
            }, 200);
        }
    }

    shouldScroll(id: string) {
        const offsetFromTop = document
            .getElementById(id)
            .getBoundingClientRect().top;
        const viewPortHeight = document.documentElement.clientHeight;
        const elementHeight = document.getElementById(id).clientHeight;

        return offsetFromTop + elementHeight > viewPortHeight;
    }
}
