import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-scan-me',
    templateUrl: './scan-me.page.html',
    styleUrls: ['./scan-me.page.scss'],
})
export class ScanMePage implements OnInit {
    defaultHref = 'about';
    constructor() {}

    ngOnInit() {
        console.log('got here');
    }
}
