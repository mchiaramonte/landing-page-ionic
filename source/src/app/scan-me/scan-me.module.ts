import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ScanMePageRoutingModule } from './scan-me-routing.module';

import { ScanMePage } from './scan-me.page';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, ScanMePageRoutingModule],
    declarations: [ScanMePage],
})
export class ScanMePageModule {}
