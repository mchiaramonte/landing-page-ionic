import {
    AfterViewInit,
    Component,
    ElementRef,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import { Platform } from '@ionic/angular';

interface Project {
    title: string;
    description: string;
    image: string;
    imageMaxWidth: number;
    path: string;
    sourceCodeUrl: string;
    liveDemoUrl: string;
    apiDefinition: string;
    technologies: string[];
    fillImgBackground: string;
}

@Component({
    selector: 'app-projects',
    templateUrl: './projects.page.html',
    styleUrls: ['./projects.page.scss'],
})
export class ProjectsPage implements OnInit {
    @ViewChild('cardcontent') projectImage: any;

    projects: Project[] = [
        {
            title: 'Digital Consultation',
            description:
                'Small demo of a medical consultation scheduling and symptom picker',
            image: 'digital-health.png',
            imageMaxWidth: null,
            path: 'digital-health',
            sourceCodeUrl: 'https://gitlab.com/mchiaramonte/well-app',
            liveDemoUrl: 'https://well.chiaramonte.me',
            apiDefinition:'https://well.chiaramonte.me/api/v1/swagger-ui',
            technologies: ['Angular', 'Spring Boot', 'OpenAPI', 'PostgreSQL'],
            fillImgBackground: null,
        },
        {
            title: 'Time tracking',
            description:
                'Small demo of a medical consultation scheduling and symptom picker',
            image: 'time-tracking.png',
            imageMaxWidth: 640,
            path: '',
            sourceCodeUrl: '',
            liveDemoUrl: 'https://timekeeping.chiaramonte.me',
            apiDefinition:'',
            technologies: ['Angular', 'Spring Boot'],
            fillImgBackground: null,
        },
        {
            title: 'Portfolio',
            description: 'Built with Ionic and Angular',
            image: 'portfolio.png',
            imageMaxWidth: 300,
            path: '',
            sourceCodeUrl: 'https://gitlab.com/mchiaramonte/landing-page-ionic',
            liveDemoUrl: 'https://chiaramonte.me',
            apiDefinition:'',
            technologies: ['Ionic'],
            fillImgBackground: null,
        },
        {
            title: 'Electric Vehicle Tracking',
            description:
                'Small demo of a medical consultation scheduling and symptom picker',
            image: 'ba-system-design.svg',
            imageMaxWidth: null,
            path: '',
            sourceCodeUrl: '',
            liveDemoUrl: '',
            apiDefinition:'',
            technologies: ['Angular', 'InfluxDB', 'Flask'],
            fillImgBackground: 'white',
        },
    ];

    constructor(public platform: Platform) {}

    ngOnInit() {
        // this.observer.observe(this.host.nativeElement);
    }

    toggleMenu() {}

    openUrl(url: string) {
        this.platform.ready().then(() => {
            window.open(url, '_blank');
        });
    }
}
