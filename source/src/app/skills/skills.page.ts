import { Component, OnInit } from '@angular/core';

interface Skill {
    name: string;
    level: number;
    icon: string;
}

@Component({
    selector: 'app-skills',
    templateUrl: './skills.page.html',
    styleUrls: ['./skills.page.scss'],
})
export class SkillsPage implements OnInit {
    maxSkillLevel = 5;

    skills: { type: string; skillList: Skill[] }[] = [
        {
            type: 'Frontend',
            skillList: [
                {
                    name: 'Angular',
                    level: 5,
                    icon: 'assets/icon/angular.svg',
                },
                {
                    name: 'HTML CSS',
                    level: 5,
                    icon: 'assets/icon/html5.svg',
                },
                {
                    name: 'Ionic',
                    level: 4,
                    icon: 'logo-ionic',
                },
            ],
        },
        {
            type: 'Backend',
            skillList: [
                {
                    name: 'Spring Boot',
                    level: 5,
                    icon: 'assets/icon/spring-boot.svg',
                },
                {
                    name: 'Hibernate',
                    level: 5,
                    icon: 'assets/icon/hibernate.svg',
                },
                {
                    name: 'Python',
                    level: 5,
                    icon: 'assets/icon/python.svg',
                },
            ],
        },
        {
            type: 'Cloud',
            skillList: [
                {
                    name: 'Kubernetes',
                    level: 4,
                    icon: 'assets/icon/kubernetes.svg',
                },
                {
                    name: 'Google Cloud',
                    level: 4,
                    icon: 'cloud',
                },
                {
                    name: 'Terraform',
                    level: 3,
                    icon: 'hammer',
                },
            ],
        },
        {
            type: 'Databases',
            skillList: [
                {
                    name: 'SQL',
                    level: 5,
                    icon: 'assets/icon/kubernetes.svg',
                },
                {
                    name: 'InfluxDB',
                    level: 4,
                    icon: 'analytics',
                },
            ],
        },
        {
            type: 'IoT Prototyping',
            skillList: [
                {
                    name: 'NodeRed',
                    level: 5,
                    icon: 'assets/icon/node-red.svg',
                },
                {
                    name: 'MicroPython',
                    level: 5,
                    icon: 'assets/icon/python.svg',
                },
            ],
        },
        {
            type: 'CAD',
            skillList: [
                {
                    name: 'Eagle PCB',
                    level: 4,
                    icon: 'grid',
                },

                {
                    name: 'FreeCad',
                    level: 3,
                    icon: 'desktop',
                },
            ],
        },
        {
            type: 'Industry 4.0',
            skillList: [
                {
                    name: 'MQTT',
                    level: 5,
                    icon: 'wifi',
                },
                {
                    name: 'OPCUA',
                    level: 4,
                    icon: 'git-compare',
                },
            ],
        },
        {
            type: 'Other',
            skillList: [
                {
                    name: 'C++',
                    level: 3,
                    icon: 'code',
                },

                {
                    name: 'C#',
                    level: 3,
                    icon: 'code',
                },
            ],
        },
    ];

    constructor() {}

    ngOnInit() {}
}
