import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'about',
        loadChildren: () =>
            import('./home/home.module').then((m) => m.HomePageModule),
    },
    {
        path: 'projects',
        loadChildren: () =>
            import('./projects/projects.module').then(
                (m) => m.ProjectsPageModule
            ),
    },
    {
        path: '',
        redirectTo: 'about',
        pathMatch: 'full',
    },
    {
        path: 'scan-me',
        loadChildren: () =>
            import('./scan-me/scan-me.module').then((m) => m.ScanMePageModule),
    },
    {
        path: 'skills',
        loadChildren: () =>
            import('./skills/skills.module').then((m) => m.SkillsPageModule),
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}
